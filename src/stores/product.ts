import { ref } from "vue";
import { defineStore } from "pinia";

import type Product from "@/type/product";

export const useProductStore = defineStore("product", () => {
  const products = ref(
    Array.from(Array(100).keys()).map((item) => {
      return {
        id: item,
        name: "Product " + (item + 1),
        price: (Math.floor(Math.random() * 100) + 1) * 10,
      };
    })
  );
  const total = ref(0);
  const pdProducts = ref(<Product[]>[]);

  function addProduct(pd: Product): void {
    pdProducts.value.push(pd);
    total.value += pd.price;
  }

  return {
    products,
    addProduct,
    total,
    pdProducts,
  };
});
